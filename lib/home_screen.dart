import 'package:explore_routes/login/profile_info.dart';
import 'package:explore_routes/map_routes.dart/map_route_screen.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatelessWidget {
  final ProfileInfo user;
  HomeScreen(this.user);

  @override
  Widget build(BuildContext context) {
    return Material(
      type: MaterialType.transparency,
      child: Container(
        color: Color(0xff1976d2),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.all(20),
              child: Text(
                "Successfully Logined as",
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 18,
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.all(20),
              child: Text(
                user.name,
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 18,
                ),
              ),
            ),
            Image.network(
              user.profileImage,
              width: 120,
              height: 120,
            ),
            RaisedButton(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20),
              ),
              color: Color(0xff004ba0),
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => RouteMapScreen()));
              },
              child: Text(
                "Open Maps",
                style: TextStyle(color: Colors.white),
              ),
            )
          ],
        ),
      ),
    );
  }
}
