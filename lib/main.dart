import 'package:explore_routes/login/login.dart';
import 'package:flutter/material.dart';

void main() => runApp(MaterialApp(
      home: LoginScreen(),
      title: "Explore Routes",
    ));
