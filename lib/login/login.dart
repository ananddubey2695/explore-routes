import 'package:explore_routes/home_screen.dart';
import 'package:explore_routes/login/login_bloc.dart';
import 'package:explore_routes/login/profile_info.dart';
import 'package:flutter/material.dart';

class LoginScreen extends StatefulWidget {
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  bool isLoggedIn = false;
  ProfileInfo user;

  void initState() {
    super.initState();
    bloc.profileInfo.listen((data) {
      if (data != null) {
        user = data;
        onLoginStatusChanged(true);
      }
    });
  }

  void onLoginStatusChanged(bool isLoggedIn) {
    setState(() {
      this.isLoggedIn = isLoggedIn;
    });
  }

  @override
  Widget build(BuildContext context) {
    String buttonInfo = "";
    if (isLoggedIn)
      buttonInfo = "Go to Details";
    else
      buttonInfo = "Facebook";
    return Material(
      type: MaterialType.transparency,
      child: Container(
        color: Color(0xff1976d2),
        child: Stack(
          children: <Widget>[
            Positioned(
              left: 1,
              right: 1,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0, 100, 0, 18),
                    child: Image.asset(
                      "assets/images/map_home_icon.png",
                      width: 120,
                      height: 120,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 18),
                    child: Text(
                      "Explore Routes",
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 18,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Positioned(
              bottom: 200,
              right: 50,
              left: 50,
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      "Sign in",
                      style: TextStyle(
                        fontSize: 14,
                        color: Colors.white,
                      ),
                    ),
                  ),
                  RaisedButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20),
                    ),
                    color: Color(0xff004ba0),
                    child: Padding(
                      padding: const EdgeInsets.all(2.0),
                      child: Row(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Image.asset(
                            "assets/images/facebook_icon.png",
                            width: 42,
                            height: 42,
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 8),
                            child: Text(
                              buttonInfo,
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 16,
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                    onPressed: () {
                      if (isLoggedIn && user != null)
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => HomeScreen(user)));
                      else
                        bloc.facebookLogin();
                    },
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
