import 'dart:convert';

import 'package:explore_routes/login/profile_info.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:rxdart/rxdart.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:http/http.dart' as http;

class LoginBloc {
  final _profileFetcher = PublishSubject<ProfileInfo>();
  FirebaseMessaging _firebaseMessaging;

  Observable<ProfileInfo> get profileInfo => _profileFetcher.stream;

  LoginBloc() {
    startNotificationService();
  }

  void startNotificationService() {
    _firebaseMessaging = FirebaseMessaging();
    _firebaseMessaging.setAutoInitEnabled(true);
    _firebaseMessaging.getToken().then((token) {
      print("Firebase Token: " + token);
    });

    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print('on message $message');
      },
      onResume: (Map<String, dynamic> message) async {
        print('on resume $message');
      },
      onLaunch: (Map<String, dynamic> message) async {
        print('on launch $message');
      },
    );
  }

  Future facebookLogin() async {
    var facebookLogin = FacebookLogin();
    var facebookLoginResult =
        await facebookLogin.logInWithReadPermissions(['email']);
    switch (facebookLoginResult.status) {
      case FacebookLoginStatus.error:
        print("Error");
        print(facebookLoginResult.errorMessage);
        break;
      case FacebookLoginStatus.cancelledByUser:
        print("CancelledByUser");
        break;
      case FacebookLoginStatus.loggedIn:
        print("LoggedIn");
        var profileResponse = await http.get(
            'https://graph.facebook.com/v2.12/me?fields=name,first_name,last_name,picture.height(200),email&access_token=${facebookLoginResult.accessToken.token}');
        Map<String, dynamic> user = jsonDecode(profileResponse.body);

        print(user);
        _profileFetcher.sink.add(ProfileInfo(user['name'].toString(),
            user['picture']['data']['url'].toString()));
        break;
    }
  }

  dispose() {
    _profileFetcher.close();
  }
}

final bloc = LoginBloc();
