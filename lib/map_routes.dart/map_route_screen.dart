import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
// import 'package:simple_permissions/simple_permissions.dart';

class RouteMapScreen extends StatefulWidget {
  // AIzaSyBESiElr5uMsmkBw7pOTQxuxYDoIIng6w4
  _RouteMapScreenState createState() => _RouteMapScreenState();
}

class _RouteMapScreenState extends State<RouteMapScreen> {
  GoogleMapController controller;

  final LatLng center = LatLng(19.1098, 72.8583);

  void onMapCreated(GoogleMapController controller) {
    this.controller = controller;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Maps"),
        ),
        body: GoogleMap(
          onMapCreated: onMapCreated,
          options: GoogleMapOptions(
            myLocationEnabled: true,
            cameraPosition: CameraPosition(
              target: center,
              zoom: 15,
            ),
          ),
        ));
  }
}
